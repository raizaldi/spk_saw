<b>Tabel Substansi Artikel - Normalisasi </b>
<table class="table table-bordered">
  <thead>
	<tr class="danger">
		<th>Nama Penulis</th>
		<th>Judul Artikel</th>
		<th>Total Nilai</th>
		<th>Nilai maximal</th>
		<th>Normalisasi</th>
		<th>Substansi</th>
	</tr>
  </thead>

 <?php
 
		include('access/db.php');
		$jawaban="SELECT a.`id_matrik`, a.`nama_penulis` as 'nama_penulis', a.`judul_artikel` as 'judul_artikel', a.`substansi` as 'substansi',
					 sum(`nilai`) as `nilai` ,(select MAX(`nilai`) from `nilai_substansi_artikel` 
					WHERE `substansi`='ARTIKEL') as `max` FROM `nilai_substansi_artikel` a WHERE `substansi`='ARTIKEL' group by `nama_penulis`";

if(!$result = $db->query($jawaban)){
die(' query error [' . $db->error . ']');
}
while($row=mysqli_fetch_array($result)){  
?>
<tr>
	<td><?=$row['nama_penulis'] ?></td>
	<td><?=$row['judul_artikel'] ?></td>
	<td><center><?=$row['nilai'] ?></center></td>
	<td><center><?=$row['max'] ?></center></td>
	<td><center><?php $nilai =  $row['nilai'] / $row['max']; 
						
					  echo round($nilai,2); ?> </center></td>
	<td><?=$row['substansi']?></td>
</tr>
<?php
 }
?>
</table>


<b>Tabel Substansi Penulisan - Normalisasi </b>
<table class="table table-bordered">
  <thead>
	<tr class="danger">
		<th>Nama Penulis</th>
		<th>Judul GAYA PENULISAN</th>
		<th>Total Nilai</th>
		<th>Nilai maximal</th>
		<th>Normalisasi</th>
		<th>Substansi</th>
	</tr>
  </thead>

 <?php
		include('access/db.php');
		$jawaban="SELECT a.`id_matrik`, a.`nama_penulis` as 'nama_penulis', a.`judul_artikel` as 'judul_artikel', a.`substansi` as 'substansi',
					 sum(`nilai`) as `nilai` ,(select MAX(`nilai`) from `nilai_gaya_penulisan` 
					WHERE `substansi`='GAYA PENULISAN') as `max` FROM `nilai_gaya_penulisan` a WHERE `substansi`='GAYA PENULISAN' group by `nama_penulis`";

if(!$result = $db->query($jawaban)){
die(' query error [' . $db->error . ']');
}
while($row=mysqli_fetch_array($result)){  
?>
<tr>
	<td><?=$row['nama_penulis'] ?></td>
	<td><?=$row['judul_artikel'] ?></td>
	<td><center><?=$row['nilai'] ?></center></td>
	<td><center><?=$row['max'] ?></center></td>
	<td><center><?php $nilai =  $row['nilai'] / $row['max']; 
					  echo round($nilai,2); ?></center></td>
	<td><?=$row['substansi']?></td>
</tr>
<?php
 }
?>
</table>




<b>Perangkingan </b>
<table class="table table-bordered">
  <thead>
	<tr class="danger">
		<th>Nama Penulis</th>
		<th>Judul</th>
		<th colspan="2"><center>Normalisasi</center></th>
		<th>Ranking</th>
		<th>Total</th>
	</tr>
  </thead>

 <?php
		include('access/db.php');
		$jawaban="SELECT a.nama_penulis as `nama`,a.judul_artikel as `judul`, a.nilai as nilai_penulisan,b.nilai nilai_artikel,
					(select MAX(`nilai`) from `nilai_substansi_artikel` 
										WHERE `substansi`='ARTIKEL') as `maksimal_artikel`,
					(select MAX(`nilai`) from `nilai_gaya_penulisan` 
										WHERE `substansi`='GAYA PENULISAN') as `maksimal_penulis`
					FROM nilai_gaya_penulisan a inner join nilai_substansi_artikel b
					on a.nama_penulis=b.nama_penulis group by  a.nama_penulis";

if(!$result = $db->query($jawaban)){
die(' query error [' . $db->error . ']');
}
while($row=mysqli_fetch_array($result)){  
?>
<tr>
	<td><?= $row['nama']?></td>
	<td><?= $row['judul']?></td>
	<td><?php $nilai1 =  $row['nilai_artikel'] / $row['maksimal_artikel']; 
					  echo round($nilai1,2); ?></td>
	<td><?php $nilai2 =  $row['nilai_penulisan'] / $row['maksimal_penulis']; 
					  echo round($nilai2,2); ?></td>
	<td>
		<?php
			$total = $nilai1+$nilai2;
			echo round($total,2) . ' x 0.5';
		?>
	</td>
	<td>
		<?php  $hasil = $total*0.5;
				echo round($hasil,2);
		?>
	</td>
</tr>
<?php
 }
?>
</table>


</table>

