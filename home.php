<br/><br/>
<table class="table table-bordered">
  <thead>
	<tr class="info">
		<th align="center">No</th>
		<th align="center">Volume Jurnal</th>
		<th align="center">Judul Artikel</th>
		<th align="center">Penulis</th>
		<th align="center">Tanggal Masuk </th>
		<th align="center">Nilai Artikel</th>
		<th align="center">Nilai Penulisan</th>
		<th><center>Penilaian</center></th>
	</tr>
  </thead>
  <?php
   include('access/db.php');
   $data = $_SESSION['volume_jurnal'];
   $sql="SELECT `judul_artikel`, `volume_jurnal`, `penulis`, `tanggal`, `abstrak`, `file_upload`, `nilai_artikel`, `nilai_penulisan` FROM `artikel` where `volume_jurnal`='$data' ";
	if(!$result = $db->query($sql)){
		die(' query error [' . $db->error . ']');
	}
	$no = 1;
	while($pilih = $result->fetch_object()){
?>
  <tbody>
	<tr align="center">
	    <td><?=$no?></td>
	    <td><?=$pilih->volume_jurnal?></td>
		<td><?=$pilih->judul_artikel?></td>
		<td><?=$pilih->penulis?></td>
		<td><?=$pilih->tanggal?></td>
		<td><?=$pilih->nilai_artikel?></td>
		<td><?=$pilih->nilai_penulisan?></td>
		<td>
			 <a href="?module=penilaian/artikel&P=<?=$pilih->penulis?>" class="btn btn-primary btn-sm" role="button">Review Artikel</a>
			 <a href="?module=penilaian/penulisan&P=<?=$pilih->penulis?>" class="btn btn-danger btn-sm" role="button">Review Penulisan</a>
			 <a href="?module=penilaian/u&P=<?=$pilih->judul_artikel?>" class="btn btn-success btn-sm" role="button">Lihat Artikel</a>
		</td>
	</tr>
  </tbody>
  <?php
  	$no++;
   }?>
</table>