<?php
 include('access/db.php');
error_reporting(0);
session_start();
if(!isset($_SESSION['username'])){
	header('location:login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Sistem Pendukung Keputusan</title>
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
	<style type='text/css'>
		body {
  padding-top: 50px;
}
.starter-template {
  padding: 40px 15px;
  text-align: left;
}
	</style>
  </head>
  <body>
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Sistem Pendukung Keputusan Metode SAW</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
		  <li><a href="?module=home">Home</a></li>
		  <li><a href="?module=hasil">Hasil Data Matrik</a></li>
		  <li><a href="logout.php">Keluar</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    <div class="container">
      <div class="row">
			<div class="starter-template">
				<?php
                        if(!isset($_GET['module'])){
                          include "home.php";
						  
						//ini untuk include ke folder subindikator
                        }else if($_GET['module']=='subindikator'){
							include "subindikator/home.php";
						}else if($_GET['module']=='hasil'){
							include "datamatrik.php";
						}else if($_GET['module']=='penilaian/artikel'){
							include "artikel.php";
						}else if($_GET['module']=='penilaian/penulisan'){
							include "penulisan.php";
						}
            else if($_GET['module']=='penilaian/u'){
              include "tampil.php";
            }
						else if($_GET['module']=='home'){
							include "home.php";
						}
					?>
	  
	  
			</div>
		</div>
    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- jQuery Version 1.11.0 -->
    <script src="assets/js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/bootstrap.min.js"></script>
    </body>
</html>