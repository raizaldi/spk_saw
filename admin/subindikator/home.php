<a class="btn btn-success" href="?module=subindikator/form" role="button">Tambah</a>
<br/><br/>
<div class="table-responsive">
<table id="dataTable" class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Kode</th>
				<th>Substansi</th>
				<th>Subunsur</th>
				<th>Indikator A</th>
				<th>Indikator B</th>
				<th>Indikator C</th>
				<th>Indikator D</th>
				<th>Indikator E</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
		<?php
			
			$sql = "SELECT `kode`, `substansi`, `sub_unsur`, `a`, `b`, `c`, `d`, `e` FROM `sub_indikator` group by `kode` asc";
			$result = $db->query($sql);
			while($row = mysqli_fetch_assoc($result)){ ?>
			<tr>
				<td><?=$row['kode'];?></td>
				<td><?=$row['substansi'];?></td>
				<td><?=$row['sub_unsur'];?></td>
				<td><?=$row['a'];?></td>
				<td><?=$row['b'];;?></td>
				<td><?=$row['c'];?></td>
				<td><?=$row['d'];;?></td>
				<td><?=$row['e'];?></td>
				<td>
					 <a href="?module=subindikator/ubah&u=<?=$row['kode'];?>" class="btn btn-primary btn-sm" role="button">Ubah</a>
					 <a href="?module=subindikator/hapus&h=<?=$row['kode'];?>" class="btn btn-danger btn-sm" role="button">Hapus</a>
				</td>
			</tr>
			<?php
			  }
			?>
		</tbody>
	</table>
</div>	
	<script type="text/javascript">
        $(document).ready(function () {
            var table = $('#dataTable').dataTable();
            var tableTools = new $.fn.dataTable.TableTools(table, {
                'aButtons': [
                    {
                        'sExtends': 'xls',
                        'sButtonText': 'Save to Excel',
                        'sFileName': 'Data.xls'
                    },
                    {
                        'sExtends': 'print',
                        'bShowAll': true,
                    },
                    {
                        'sExtends': 'pdf',
                        'bFooter': false
                    },
                    'copy',
                    'csv'
                ],
                'sSwfPath': '//cdn.datatables.net/tabletools/2.2.4/swf/copy_csv_xls_pdf.swf'
            });
            $(tableTools.fnContainer()).insertBefore('#dataTable_wrapper');
        });
    </script>

