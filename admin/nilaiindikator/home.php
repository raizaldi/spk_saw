<a class="btn btn-success" href="?module=nilaiindikator/form" role="button">Tambah</a>
<br/><br/>
<table id="dataTable" class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>No</th>
				<th>Kode</th>
				<th>Nilai A</th>
				<th>Nilai B</th>
				<th>Nilai C</th>
				<th>Nilai D</th>
				<th>Nilai E</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
		<?php
			
			$sql = "SELECT `id_nilai_indikator`, `kode`, `nilaia`, `nilaib`, `nilaic`, `nilaid`, `nilaie` FROM `nilai_indikator`";
			$result = $db->query($sql);
			while($row = mysqli_fetch_assoc($result)){ ?>
			<tr>
				<td><?=$row['id_nilai_indikator'];?></td>
				<td><?=$row['kode'];?></td>
				<td><?=$row['nilaia'];?></td>
				<td><?=$row['nilaib'];;?></td>
				<td><?=$row['nilaic'];?></td>
				<td><?=$row['nilaid'];;?></td>
				<td><?=$row['nilaie'];?></td>
				<td>
					 <a href="?module=nilaiindikator/ubah&u=<?=$row['kode'];?>" class="btn btn-primary btn-sm" role="button">Ubah</a>
					 <a href="?module=nilaiindikator/hapus&h=<?=$row['kode'];?>" class="btn btn-danger btn-sm" role="button">Hapus</a>
				</td>
			</tr>
			<?php
			  }
			?>
		</tbody>
	</table>
	
	<script type="text/javascript">
        $(document).ready(function () {
            var table = $('#dataTable').dataTable();
            var tableTools = new $.fn.dataTable.TableTools(table, {
                'aButtons': [
                    {
                        'sExtends': 'xls',
                        'sButtonText': 'Save to Excel',
                        'sFileName': 'Data.xls'
                    },
                    {
                        'sExtends': 'print',
                        'bShowAll': true,
                    },
                    {
                        'sExtends': 'pdf',
                        'bFooter': false
                    },
                    'copy',
                    'csv'
                ],
                'sSwfPath': '//cdn.datatables.net/tabletools/2.2.4/swf/copy_csv_xls_pdf.swf'
            });
            $(tableTools.fnContainer()).insertBefore('#dataTable_wrapper');
        });
    </script>

