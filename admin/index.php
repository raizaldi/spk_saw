<?php
 include('../access/db.php');
error_reporting(0);
session_start();
if(!isset($_SESSION['username']) || $_SESSION['level']!="administrator"){
	header('location:login.php');
}
?>
<!DOCTYPE html>
<html>
<head>
    <title><?= $_SESSION['level'] ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
	<link rel="stylesheet" type="text/css" href="../asset/css/semantic.css">
    <link rel="stylesheet" type="text/css" href="../asset/css/main.css">
    <link rel="stylesheet" type="text/css" href="../asset/css/animate.css">
   <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
   
	<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/9dcbecd42ad/integration/bootstrap/3/dataTables.bootstrap.css">
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/tabletools/2.2.4/css/dataTables.tableTools.css" />
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
 
	<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/9dcbecd42ad/integration/bootstrap/3/dataTables.bootstrap.js"></script>
	<script type="text/javascript" language="javascript" src="//cdn.datatables.net/tabletools/2.2.4/js/dataTables.tableTools.min.js"></script>

</head>

<body>
    <div class="ui fixed top menu">
       <a href="index.php" data-toggle="slide" class="item"><h3>Selamat datang <?= $_SESSION['username'] ?> </h3></a>
        <div class="menu right">
        </div>
    </div>
    <div class="pusher">
        <div class="ui fluid container">
            <div class="ui two column grid">
                <div class="side menu" id="sideMenu">
                    <div align="center" class="profile">
                        <br>
                       
                        <div class="ui white header">Sistem Pendukung Keputusan</div>
                        <div class="ui divider"></div>
                    </div>
                    <div class="ui fluid vertical menu" id="verticalMenu">
                        <a href="?module=home" class="item"><i class="dashboard icon"></i> <span>Dashboard</span></a>
                        <a href="?module=subindikator" class="item"><i class="book icon"></i> <span>subindikator</span></a>
						<a href="?module=nilaiindikator" class="item"><i class="book icon"></i> <span>Nilai Indikator</span></a>
						<a href="?module=jurnal" class="item"><i class="book icon"></i> <span>Jurnal</span></a>
						<a href="?module=artikel" class="item"><i class="book icon"></i> <span>Artikel</span></a>
						<a href="?module=users" class="item"><i class="book icon"></i> <span>Users</span></a>
                        <a href="?module=keluar" class="item"><i class="block layout icon"></i> <span>Logout</span></a>
                    </div>
                </div>
                <div class="sixteen wide column" id="content">
                  <?php
                        if(!isset($_GET['module'])){
                          include "home.php";
						  
						//ini untuk include ke folder subindikator
                        }else if($_GET['module']=='subindikator'){
							include "subindikator/home.php";
						}else if($_GET['module']=='subindikator/form'){
							include "subindikator/form.php";
						}else if($_GET['module']=='subindikator/ubah'){
							include "subindikator/ubah.php";
						}else if($_GET['module']=='subindikator/hapus'){
							include "subindikator/hapus.php";
						
						//ini untuk include ke folder nilai indikator
						}else if($_GET['module']=='nilaiindikator'){
							include "nilaiindikator/home.php";
						}else if($_GET['module']=='nilaiindikator/form'){
							include "nilaiindikator/form.php";
						}else if($_GET['module']=='nilaiindikator/ubah'){
							include "nilaiindikator/ubah.php";
						}else if($_GET['module']=='nilaiindikator/hapus'){
							include "nilaiindikator/hapus.php";
							
						//ini untuk include ke folder users
						}else if($_GET['module']=='users'){
							include "users/home.php";
						}else if($_GET['module']=='users/form'){
							include "users/form.php";
						}else if($_GET['module']=='users/ubah'){
							include "users/ubah.php";
						}else if($_GET['module']=='users/hapus'){
							include "users/hapus.php";
						}
						//ini untuk include ke folder artikel
						else if($_GET['module']=='artikel'){
							include "artikel/home.php";
						}else if($_GET['module']=='artikel/form'){
							include "artikel/form.php";
						}else if($_GET['module']=='artikel/tampil'){
							include "artikel/tampil.php";
						}else if($_GET['module']=='artikel/hapus'){
							include "artikel/hapus.php";
						}
						
						//ini untuk include ke folder jurnal
						else if($_GET['module']=='jurnal'){
							include "jurnal/home.php";
						}else if($_GET['module']=='jurnal/form'){
							include "jurnal/form.php";
						}else if($_GET['module']=='jurnal/ubah'){
							include "jurnal/ubah.php";
						}else if($_GET['module']=='jurnal/hapus'){
							include "jurnal/hapus.php";
						}
						else if($_GET['module']=='jurnal/artikel'){
							include "jurnal/homejurnal.php";
						}
						else if($_GET['module']=='home'){
							include "home.php";
						}
						else if($_GET['module']=='keluar'){
							include "logout.php";
						}
					?>
		</div>
            </div>
        </div>
    </div>

 <script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>
