-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 22 Jun 2016 pada 17.40
-- Versi Server: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sipakar`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `artikel`
--

CREATE TABLE IF NOT EXISTS `artikel` (
  `judul_artikel` varchar(100) NOT NULL,
  `volume_jurnal` varchar(50) NOT NULL,
  `penulis` varchar(35) NOT NULL,
  `tanggal` date NOT NULL,
  `abstrak` text NOT NULL,
  `file_upload` varchar(255) NOT NULL,
  `nilai_artikel` double NOT NULL,
  `nilai_penulisan` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `artikel`
--

INSERT INTO `artikel` (`judul_artikel`, `volume_jurnal`, `penulis`, `tanggal`, `abstrak`, `file_upload`, `nilai_artikel`, `nilai_penulisan`) VALUES
('sistem a1', '1', 'A1', '2016-06-15', 'Sistem A1', 'modul-dewa89s-ahp-dan-topsis-pemilihan-beasiswa.pdf', 0, 0),
('sistem a2', '1', 'A2', '2016-06-15', 'Sistem A2', 'Skripsi Sistem Pakar - Desain dan Analisis Sistem Pakar Trouble Shooting Kerusakan PC berbasis Web PHP.pdf', 0, 0),
('sistem a3', '1', 'A3', '2016-06-15', 'Sistem A3', 'Fuzzy dan SAW.pdf', 0, 0),
('sistem a4', '1', 'A4', '2016-06-15', 'Sistem A4', 'Tutorial JQuery AJAX1.pdf', 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jurnal`
--

CREATE TABLE IF NOT EXISTS `jurnal` (
  `volume_jurnal` varchar(50) NOT NULL,
  `tgl_masuk` date NOT NULL,
  `nama_jurnal` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jurnal`
--

INSERT INTO `jurnal` (`volume_jurnal`, `tgl_masuk`, `nama_jurnal`) VALUES
('1', '2016-06-15', 'Media riset bisnis dan management');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilai_gaya_penulisan`
--

CREATE TABLE IF NOT EXISTS `nilai_gaya_penulisan` (
`id_matrik` int(11) NOT NULL,
  `nama_penulis` varchar(50) NOT NULL,
  `judul_artikel` varchar(100) NOT NULL,
  `substansi` varchar(30) NOT NULL,
  `nilai` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilai_indikator`
--

CREATE TABLE IF NOT EXISTS `nilai_indikator` (
`id_nilai_indikator` int(11) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `nilaia` double NOT NULL,
  `nilaib` double NOT NULL,
  `nilaic` double NOT NULL,
  `nilaid` double NOT NULL,
  `nilaie` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nilai_indikator`
--

INSERT INTO `nilai_indikator` (`id_nilai_indikator`, `kode`, `nilaia`, `nilaib`, `nilaic`, `nilaid`, `nilaie`) VALUES
(1, 'C.1.1', 4, 3, 2, 1, 0),
(2, 'C.1.2', 6, 4, 3, 1, 0),
(3, 'C.1.3', 6, 4, 2, 0, 0),
(4, 'C.1.4', 3, 2, 1, 0, 0),
(5, 'C.1.5', 5, 4, 3, 1, 0),
(6, 'C.1.6', 4, 2, 1, 0, 0),
(7, 'C.1.7', 5, 3, 1, 0, 0),
(8, 'C.1.8', 3, 2, 1, 0, 0),
(9, 'C.1.9', 3, 2, 1, 0, 0),
(10, 'C.2.1', 1, 0.5, 0, 0, 0),
(11, 'C.2.2', 1, 0.5, 0, 0, 0),
(12, 'C.2.3', 2, 1, 0.5, 0, 0),
(13, 'C.2.4', 1, 0.5, 0, 0, 0),
(14, 'C.2.5', 1, 0.5, 0, 0, 0),
(15, 'C.2.6', 1, 0.5, 0, 0, 0),
(16, 'C.2.7', 1, 0.5, 0, 0, 0),
(17, 'C.2.8', 2, 1, 0, 0, 0),
(18, 'C.2.9', 2, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilai_substansi_artikel`
--

CREATE TABLE IF NOT EXISTS `nilai_substansi_artikel` (
`id_matrik` int(11) NOT NULL,
  `nama_penulis` varchar(50) NOT NULL,
  `judul_artikel` varchar(100) NOT NULL,
  `substansi` varchar(30) NOT NULL,
  `nilai` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sub_indikator`
--

CREATE TABLE IF NOT EXISTS `sub_indikator` (
  `kode` varchar(10) NOT NULL,
  `substansi` varchar(30) DEFAULT NULL,
  `sub_unsur` varchar(200) DEFAULT NULL,
  `a` varchar(200) NOT NULL,
  `b` varchar(200) NOT NULL,
  `c` varchar(200) NOT NULL,
  `d` varchar(200) NOT NULL,
  `e` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sub_indikator`
--

INSERT INTO `sub_indikator` (`kode`, `substansi`, `sub_unsur`, `a`, `b`, `c`, `d`, `e`) VALUES
('C.1.1', 'ARTIKEL', 'Cakupan Keilmuan', 'Superspesialis,Misalnya: taksonomi,jamur,atau studi Jepang', 'Spesialis,Misalnya: fisiologi tumbuhan atau ekologi pesisir, atau studi asia timur', 'Cabang Ilmu, Misalnya: botani atau studi wilayah', 'Disiplin ilmu, Misalnya: Biologi atau sosiologi', 'Bunga rampai dan kombinasi berbagai disiplin ilmu, Misalnya: MIPA, sains dan ketertarikan'),
('C.1.2', 'ARTIKEL', 'Aspirasi Wawasan', 'Internasional', 'Regional', 'Nasional', 'Kawasan', 'Lokal'),
('C.1.3', 'ARTIKEL', 'Kepioniran Ilmiah/Orisinalitas Karya', 'Memuat artikel yang berisi original dan mempunyai kebahuruan/memberikannya kontribusi ilmiah tinggi', 'Memuat artikel yang berisi karya orisinal dan mempunyai pembahuruan/memberikan kontribusi ilmiah cukup', 'Memuat artikel yang berisi original dan mempunyai kebahuruan/memberikannya kontribusi ilmiah rendah', 'Memuat artikel yang berisi original dan mempunyai kebahuruan/memberikannya kontribusi ilmiah', ''),
('C.1.4', 'ARTIKEL', 'Makna Sumbangan Bagi kemajuan ilmu', 'sangat nyata', 'nyata', 'tidak nyata', '', ''),
('C.1.5', 'ARTIKEL', 'dampak ilmiah', 'tinggi (jumlah sitasi >25)', 'cukup(jumlah sitasi 11-25)', 'sedang (jumlah sitasi 6-10)', 'kurang (jumlah sitasi 1-5)', 'tidak berdampak (jumlah sitasi 0)'),
('C.1.6', 'ARTIKEL', 'nisbah sumber acuan perimer berbanding sumber lainnya', '>80%', '40-80%', '<40%', '', ''),
('C.1.7', 'ARTIKEL', 'derajat kemuktahiran', '>80%', '40-80%', '<40%', '', ''),
('C.1.8', 'ARTIKEL', 'analisis dan sintesis', 'baik', 'cukup', 'kurang', '', ''),
('C.1.9', 'ARTIKEL', 'penyimpulan dan perampatan', 'baik', 'cukup', 'kurang', '', ''),
('C.2.1', 'GAYA PENULISAN', 'Keefektifan judul artikel', 'Luas dan informatif', 'Lugas tetapi kurang informatif atau sebaliknya', 'tidak lugas dan tidak informatif', '', ''),
('C.2.2', 'GAYA PENULISAN', 'Pencantuman nama penulis dan lembaga penulis', 'Lengkap dan konsisten', 'Lengkap tapi tidak konsisten', 'Tidak lengkap dan tidak konsisten', '', ''),
('C.2.3', 'GAYA PENULISAN', 'Abstrak', 'Abstrak yang jelas dan ringkas dalam bahasa inggris dan/atau bahasa indonesia', 'Abstrak kurang jelas dan ringkas atau hanya dalam bahasa inggris atau dalam bahasa indonesia saja', 'Abstrak tidak jelas dan bahasa tidak baku', '', ''),
('C.2.4', 'GAYA PENULISAN', 'kata kunci', 'ada, konsisten dan menceriminkan konsep penting dalam artikel', 'ada tetapi kurang konsisten atau kurang mencerminkan konsep penting dalam artikel', 'Tidak ada', '', ''),
('C.2.5', 'GAYA PENULISAN', 'Sistematika pembaban', 'Lengkap dan tersistem baik', 'Lengkap tetapi tidak bersistem baik', 'Kurang lengkap dan tidak bersistem', '', ''),
('C.2.6', 'GAYA PENULISAN', 'Pemanfaatn instrumen pendukung', 'Informatif dan komplementer', 'Kurang informatif atau komplementer', 'Tak termanfaatkan', '', ''),
('C.2.7', 'GAYA PENULISAN', 'Cara pengacuan dan pengutipan', 'Baku dan konsisten dan menggunakan aplikasi pengutipan standar', 'Baku dan konsisten tetapi tidak menggunakan aplikasi pengutipan standar', 'Tidak baku dan tidak konsisten', '', ''),
('C.2.8', 'GAYA PENULISAN', 'Penyusunan daftar pustaka', 'Baku dan konsisten dan menggunakan aplikasi pengutipan standar', 'Baku dan konsisten, tetapi tidak menggunakan aplikasi pengutipan standar', 'Tidak baku dan tidak konsisten', '', ''),
('C.2.9', 'GAYA PENULISAN', 'Peristilahan dan Kebahasaan', 'berbahasa indonesia atau berbahasa resmi PBB yang baik dan benar', 'berbahasa indonesia atau berbahasa resmi PBB yang cukup baik dan benar', 'Berbahasa yang buruk', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `level`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'administrator'),
(2, 'review', '1c67665285fb6a7d761414e12578e574', 'reviewer'),
(3, 'raizaldi', '21232f297a57a5a743894a0e4a801fc3', 'administrator'),
(4, 'anisa', '40cc8f68f52757aff1ad39a006bfbf11', 'reviewer');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
 ADD PRIMARY KEY (`judul_artikel`);

--
-- Indexes for table `jurnal`
--
ALTER TABLE `jurnal`
 ADD PRIMARY KEY (`volume_jurnal`);

--
-- Indexes for table `nilai_gaya_penulisan`
--
ALTER TABLE `nilai_gaya_penulisan`
 ADD PRIMARY KEY (`id_matrik`);

--
-- Indexes for table `nilai_indikator`
--
ALTER TABLE `nilai_indikator`
 ADD PRIMARY KEY (`id_nilai_indikator`);

--
-- Indexes for table `nilai_substansi_artikel`
--
ALTER TABLE `nilai_substansi_artikel`
 ADD PRIMARY KEY (`id_matrik`);

--
-- Indexes for table `sub_indikator`
--
ALTER TABLE `sub_indikator`
 ADD PRIMARY KEY (`kode`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `nilai_gaya_penulisan`
--
ALTER TABLE `nilai_gaya_penulisan`
MODIFY `id_matrik` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `nilai_indikator`
--
ALTER TABLE `nilai_indikator`
MODIFY `id_nilai_indikator` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `nilai_substansi_artikel`
--
ALTER TABLE `nilai_substansi_artikel`
MODIFY `id_matrik` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
