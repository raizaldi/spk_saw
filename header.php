<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">


    <title>Sistem Pendukung Keputusan</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
	<style type='text/css'>
		body {
  padding-top: 50px;
}
.starter-template {
  padding: 40px 15px;
  text-align: left;
}
	</style>
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Sistem Pendukung Keputusan Metode SAW</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
		  <li><a href="index.php">Home</a></li>
		  <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reviwer<span class="caret"></span></a>
              <ul class="dropdown-menu">
			    <li><a href="artikel.php">Artikel</a></li>
                <li><a href="penulisan.php">Gaya Penulisan</a></li>
              </ul>
		  <li><a href="datamatrik.php">Hasil Data Matrik</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">
      <div class="row">
      <div class="starter-template">